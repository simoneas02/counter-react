import React, { useState } from 'react';
import { GrAddCircle, GrSubtractCircle, GrPowerReset } from 'react-icons/gr';

import './App.css';

function App() {
  const [count, setCount] = useState(1);

  return (
    <div className="App">
      <h1>{count}</h1>
      <button onClick={() => setCount(count + 1)}>
        <GrAddCircle />
      </button>

      <button onClick={() => setCount(0)}>
        <GrPowerReset />
      </button>

      <button onClick={() => setCount(count > 0 ? count - 1 : 0)}>
        <GrSubtractCircle />
      </button>
    </div>
  );
}

export default App;
